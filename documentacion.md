# Documentación Ebit Market

## Indice
- [Requerimientos](#requerimientos)
- [Estructura de archivos](#estructura-de-archivos)
- [Instalación](#instalación)
- [Iniciar App](#iniciar-app)

## Requerimientos

- Entorno React Native. [Guia](https://reactnative.dev/docs/environment-setup)
- Yarn CLI
- Expo CLI

## Estructura de archivos

:::image type="content" source="estructura-de-archivos.jpg" alt-text="estructura-de-archivos":::

## Instalación

Para instalar el proyecto hay que correr los siguientes comandos en la raiz del proyecto:
- yarn install
 
[Videotutorial: Instalación](instalacion.mp4)

## Iniciar App

#### ADV

Iniciar Simulador, correr comando "expo start" en la raiz del proyecto, al abrise la ventana de expo en el navegador hacer click en "Run on android device/emulator"

#### Dispositivo

Instalar Expo App correr comando "expo start" en la raiz del proyecto, al abrise la ventana de expo en el navegador y escanear el codigo QR con el app de EXPO

[Videotutorial: iniciar aplicación](run.mp4)

## Nociones

##### Componentes
Ruta: app/components/
[Videotutorial: componentes](components.mp4)

##### API y servicios
[Videotutorial: api y servicios](api.mp4)

##### Login
Ruta: app/screens/Login/
[Videotutorial: login](Login.mp4)

##### Registro
Ruta: app/screens/Register/
[Videotutorial: registro](Register.mp4)

##### Olvido su contraseña
Ruta: app/screens/ForgotPassword/
[Videotutorial: olvido su contrasela](ForgotPassword.mp4)

##### Inicio
Ruta: app/screens/Home/
[Videotutorial: inicio](Home.mp4)

##### Perfil
Ruta: app/screens/Profile/
[Videotutorial: perfil](Profile.mp4)

##### Estado de cuenta
Ruta: app/screens/AccountState/
[Videotutorial: estado de cuenta](AccountState.mp4)

##### Enviar ebits
Ruta: app/screens/SendEbit/
[Videotutorial: enviar ebits](SendEbit.mp4)
